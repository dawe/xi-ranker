# Xi Ranker

Paper [HERE](http://www.nature.com/nmeth/journal/v10/n12/abs/nmeth.2728.html).

## Usage
```
usage: xi.app [<options>] <network_file>

 By default it prints the results inside the csi_rank.report file.

 Options:
    -p <parser> Specify the input format. Supported formats are csv. (LOL). Default csv.
    -m <metric> Specify which metric to use. Supported metrics are pcc, jaccard. Default pcc.
    -t <threshold> Specify the threshold for the CSI formula. Default 0.05.

 e.g.:
    ./xi.app -p csv -t 0.1 -m jaccard <file_path>
```

The input file is a network with the following format
```
gene0,annotation0,annotation1
gene1, annotation0, annotation2
[...]
```

Spaces don't matter.

## Compilation
```$ make```

## Test

### Test compilation
```$ make run_test.app```

### Run
```$ make t```

## TODO
+ [ ] Better and more detailed tests
+ [ ] Add new distances
+ [ ] multiple list processing
+ [ ] implement score reduce functions for a list
+ [ ] implement score reduce functions across lists
+ [ ] implement ranking between genes trainig lists and genes set
+ [ ] add output formatters
+ [ ] flag that says whether report scores of the metric instead of the distance ones
+ [ ] make csv parser delimeter user definable
