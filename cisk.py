#/usr/bin/env python -W ignore

import sys
import argparse
import math
from operator import itemgetter

def CSI(G, X_nodes, id1, id2, metric = 'pcc', Y_nodes = set()):
  # G is a dictionary G[gene]->(set of annotations)
  # X_nodes is the list of genes in experiment (violet in Bass paper, fig1C)
  # id1, id2 are two genes (included in X_nodes)
  X_nodes = set(X_nodes)
  if id1 not in X_nodes or id2 not in X_nodes:
    return 0.0
  dist_ab = distance(G, X_nodes, id1, id2, metric = metric, Y_nodes = Y_nodes)
  thr = dist_ab - 0.05
  pass_nodes = [x for x in X_nodes if x != id1 and x != id2 and distance(G, X_nodes, id1, x) < thr and distance(G, X_nodes, id2, x) < thr]
  return 1. * len(pass_nodes)  / len(X_nodes)


def distance(G, X_nodes, id1, id2, metric = 'pcc', Y_nodes = set()):
  # G is a dictionary G[gene]->(set of annotations)
  # X_nodes is the list of genes in experiment (violet in Bass paper, fig1C)
  # id1, id2 are two genes (included in X_nodes)
  X_nodes = set(X_nodes)
  try:
    Na = G[id1]
  except KeyError:
    Na = set()
  try:
    Nb = G[id2]
  except KeyError:
    Nb = set()
  if metric == 'jaccard':
    return 1. * len(Na.intersection(Nb)) / len(Na.union(Nb))
  elif metric == 'geometric':
    return 1. * (len(Na.intersection(Nb))**2) / (len(Na) * len(Nb))
  elif metric == 'cosine':
    return 1. * len(Na.intersection(Nb)) / math.sqrt(len(Na) * len(Nb))
  elif metric == 'simpson':
    return 1. * len(Na.intersection(Nb)) / min([len(Na), len(Nb)])   
  elif metric == 'pcc':  
    if len(Y_nodes) == 0:
      for x in X_nodes:
        try:
          Y_nodes = Y_nodes.union(G[x])
        except KeyError:
          continue
    d_Y = len(Y_nodes)
    int_ab = Na.intersection(Nb)
    d_a = len(Na)
    d_b = len(Nb)
    d_int = len(int_ab)
    try:      
      return (d_int * d_Y - (d_a * d_b)) / math.sqrt(d_a * d_b * (d_Y - d_a) * (d_Y - d_b))
    except ZeroDivisionError:
      return 0.0  

#def test_CSI(G, train_set, test_gene, metric='pcc'):
#  # G is a dictionary G[gene]->(set of annotations)
#  # train_set and test_gene are two gene lists
#  CSI_list = []
#  whole_set = set(train_set)
#  whole_set.add(test_gene)
#  CSI_list = [CSI(G, whole_set, x, test_gene) for x in train_set]
#  CSI_list = [math.acos(x) for x in CSI_list]
#  m_value = 1. * sum(CSI_list) / len(CSI_list)
#  return math.cos(m_value)
#
#
#def main():
#  # parse options
#  option_parser = argparse.ArgumentParser(
#  description="Rank Genes according to CSI score",
#  prog="CSIk.py",
#  epilog="For any question, write to cittaro.davide@hsr.it")
#  option_parser.add_argument("--version", action="version", version="%(prog)s 0.1")
#  option_parser.add_argument("--train", help="A file containing genes identifying a function", action='store', type=file, required=True)
#  option_parser.add_argument("--test", help="A file containing genes to be tested", action='store', type=file, required=True)
#  option_parser.add_argument("--annotation", help="Annotation file in GMT format", action='store', type=file, required=True)
#  option_parser.add_argument("--metric", help="Metric used in distance calculation", action='store', default='pcc')
#  option_parser.add_argument("--sorted", help="Sort results by score", action="store_true", default=False)
#  option_parser.add_argument("--use-training-only", help="Only use training set genes for score calculation", action="store_true", default=False)
#
#  options = option_parser.parse_args()
#  
#  training_genes = set([])
#  test_genes = set([])
#  functional_annotation = {}
#  for line in options.train:
#    training_genes.add(line.strip())
#  
#  for line in options.test:
#    test_genes.add(line.strip())
#  
#  for line in options.annotation:
#    tmp = line.strip().split('\t')
#    ann_id = tmp[0]
#    gene_list = tmp[2:]
#    for gene in gene_list:
#      try:
#        functional_annotation[gene].add(ann_id)
#      except KeyError:
#        functional_annotation[gene] = set([ann_id])
#  
#  score_genes = training_genes
#  if not options.use_training_only:
#    score_genes = score_genes.union(test_genes)
#  compound_CSI_values = [test_CSI(functional_annotation, score_genes, x, metric=options.metric) for x in test_genes]
#  test_genes = list(test_genes)
#  if options.sorted:
#    out_data = sorted([(compound_CSI_values[x], test_genes[x]) for x in range(len(test_genes))], key=itemgetter(0), reverse=True)
#  else:
#    out_data = [(compound_CSI_values[x], test_genes[x]) for x in range(len(test_genes))]
#  for (score, gene) in out_data:
#    sys.stdout.write("%s\t%.3e\n" % (gene, score))    
#        
#
#
#if __name__ == '__main__':
#  main()