#ifndef TSVXIPARSER_H
#define TSVXIPARSER_H

#include "abstract_xi_parser.hpp"

class TsvXiParser : public AbstractXiParser {

public:
    TsvXiParser();
    TsvXiParser(network_t* collection);
    ~TsvXiParser();
    void virtual parse(std::ifstream& input);
    void virtual parse_line(const std::string& line);
    network_t* results() const;

private:
    network_t* co;
};

#endif
