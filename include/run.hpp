#ifndef XI_RUN_H
#define XI_RUN_H

#include <fstream>
#include <iostream>
#include "csv_xi_parser.hpp"
#include "tsv_xi_parser.hpp"
#include "xi_ranker.hpp"
#include "csi_distance.hpp"
#include "pcc_distance.hpp"
#include "jaccard_distance.hpp"

#endif
