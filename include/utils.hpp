#ifndef XI_UTILS_H
#define XI_UTILS_H

#include <algorithm>
#include <functional>
#include <cctype>
#include <locale>
#include <vector>
#include <sstream>

inline std::string &ltrim(std::string &s) {
        s.erase(s.begin(), std::find_if(s.begin(), s.end(),
            std::not1(std::ptr_fun<int, int>(std::isspace))));
        return s;
}

// trim from end
inline std::string &rtrim(std::string &s) {
        s.erase(std::find_if(s.rbegin(), s.rend(),
            std::not1(std::ptr_fun<int, int>(std::isspace))).base(), s.end());
        return s;
}

// trim from both ends
inline std::string &trim(std::string &s) {
        return ltrim(rtrim(s));
}

inline std::vector<std::string>&
split(const std::string& s, std::vector<std::string>& tokens, const char& delim) {
    std::stringstream ss(s);
    std::string t;
    while(std::getline(ss, t, delim)) {
        tokens.push_back(t);
    }

    return tokens;
}

inline void
intersection(const y_node_list_t& a, const y_node_list_t& b, y_node_list_t& i) {
    std::set_intersection(a.begin(), a.end(), b.begin(), b.end(), i.begin());
    unsigned z = 0;
    for (y_node_list_t::const_iterator it = i.begin(); it < i.end(); ++it, ++z) {
        if (it->empty())
            break;
    }
    i.resize(z);
}

inline void
uni(const y_node_list_t& a, const y_node_list_t& b, y_node_list_t& u) {
    std::set_union(a.begin(), a.end(), b.begin(), b.end(), u.begin());
    unsigned z = 0;
    for (y_node_list_t::const_iterator it = u.begin(); it < u.end(); ++it, ++z) {
        if (it->empty())
            break;
    }
    u.resize(z);
}

// void inline
// comp_scores(Distance& d, const network_t& nt, score_list_t& scores) {
//     #ifndef NDEBUG
//         std::cout << " static void comp_scores(Distance& d, const network_t& nt, score_list_t& scores)" << std::endl;
//         std::cout << " static void comp_scores(Distance& d, const network_t& nt, score_list_t& scores) co->size() = " << nt.size() << " scores.size() = " << scores.size() << std::endl;
//     #endif

//     // For each pair of nodes, compute the distance
//     network_t::const_iterator xita, xitb, xend;

//     for (xita = nt.begin(), xend = nt.end(); xita != xend; ++xita) {
//         xitb = xita + 1;
//         for (; xitb != xend; ++xitb) {
//             scores.push_back(x_nodes_pair_t(xita->node,
//                                           xitb->node,
//                                           d(nt, *xita, *xitb)));
//         }
//     }
// }


#endif