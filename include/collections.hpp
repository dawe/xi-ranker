#ifndef COLLECTIONS_H
#define COLLECTIONS_H

#include <vector>
#include <list>
#include <string>
#include <iostream>

typedef std::string x_node_t;
typedef std::string y_node_t;
typedef std::vector<y_node_t> y_node_list_t;

struct adj_t {
    x_node_t node;
    y_node_list_t adj;
};

struct adj_pair {
    const adj_t *a, *b;
    adj_pair(): a(0), b(0) {}
    adj_pair(const adj_t& aa, const adj_t& bb) : a(&aa), b(&bb) {}
    adj_pair(const adj_pair& o) : a(o.a), b(o.b) {}
    inline bool operator==(const adj_pair& o) const {
        return ! this->operator!=(o);
    }
    inline bool operator!=(const adj_pair& o) const {
        return a != o.a && b != o.b;
    }
    inline bool is_a(const adj_t& adj) const {
        return a == &adj;
    }
    inline bool is_b(const adj_t& adj) const {
        return b == &adj;
    }
};

// namespace std {
//     template <>
//     struct hash<adj_pair> {
//         typedef adj_pair argument_type;
//         typedef std::size_t result_type;

//         result_type operator()(const adj_pair& p) const {
//             hash<string> h;
//             return h(p.a->node) ^ h(p.b->node);
//         }
//     };
// };

inline std::ostream& operator<< (std::ostream& o, const adj_pair& a) {
    // easy to manipulate programmatically
    o << a.a->node << "," << a.b->node;
    return o;
}

inline std::ostream& operator<< (std::ostream& o, const adj_t& a) {
    // easy to manipulate programmatically
    std::string d(",");
    o << a.node;
    for (y_node_list_t::const_iterator i = a.adj.begin(), e = a.adj.end();
        i != e;
        ++i)
    { o << d << (*i); }

    return o;
}

typedef std::list<adj_t> adj_list_t;

struct network_t {
    adj_list_t nt;
    struct cpair_iterator {
        adj_list_t::const_iterator i, j, end;
        adj_pair p;
        cpair_iterator() {}
        cpair_iterator(adj_list_t::const_iterator b, adj_list_t::const_iterator e)
            : i(b), j(++b), end(e), p(*i, *j) { }
        cpair_iterator(const cpair_iterator& o) : i(o.i), j(o.j), end(o.end), p(o.p) {}

        cpair_iterator& operator=(const cpair_iterator& o) {
            if (this == &o) return *this;
            i = o.i; j = o.j; p = o.p; end = o.end;
            return *this;
        }

        const adj_pair& operator*() const { return p; }
        adj_pair* operator->() { return &p; }

        cpair_iterator& operator++() {
            if (++j == end) {
                ++i; j = i; ++j; // j = i + 1
            }
            p = adj_pair(*i, *j);
            return *this;
        }

        cpair_iterator operator++(int) {
            cpair_iterator tmp(*this);
            operator++();
            return tmp;
        }

        bool operator==(const cpair_iterator& o) const {
            return i == o.i && j == o.j;
        }

        bool operator!=(const cpair_iterator& o) const {
            return ! this->operator==(o);
        }
    };
    cpair_iterator begin() const {
        return cpair_iterator(nt.begin(), nt.end());
    }
    cpair_iterator end() const {
        return cpair_iterator(--nt.end(), nt.end());
    }
};

#endif