#ifndef JACCARD_H
#define JACCARD_H

#include "distance.hpp"
#include "utils.hpp"

class Jaccard : public Distance {

public:
    Jaccard();
    virtual ~Jaccard();
    double operator()(const network_t& nt, const adj_t& n1, const adj_t& n2);
};

#endif