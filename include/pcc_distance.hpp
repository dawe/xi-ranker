#ifndef PCC_H
#define PCC_H

#include <cmath>
#include "distance.hpp"
#include "utils.hpp"

class Pcc : public Distance {
public:
    Pcc();
    virtual ~Pcc();
    double operator()(const network_t& nt, const adj_t& n1, const adj_t& n2);
private:
    int total_y_nodes;
};


#endif