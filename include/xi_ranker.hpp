#ifndef XIRANKER_H
#define XIRANKER_H

#include <fstream>
#include <algorithm>
#include "collections.hpp"
#include "abstract_xi_parser.hpp"
#include "distance.hpp"
#include "utils.hpp"


class XiRanker {

public:
    XiRanker();
    XiRanker(const std::string& file_name);
    ~XiRanker();

    void parser(AbstractXiParser& p);
    void distance(Distance& dist);
    void run();
    void clear();
    void print_results(std::ostream& out);
    void comp_scores(Distance& m, const network_t& co, score_list_t& scores);

private:
    std::string m_input_file_name;
    Distance* m_d;
    AbstractXiParser* m_parser;
    score_list_t* m_scores;
    static const std::string m_output_file_name;
};

#endif