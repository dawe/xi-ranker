#ifndef ABSTRACTXIPARSER_H
#define ABSTRACTXIPARSER_H

#include "xi_parser_interface.hpp"

class AbstractXiParser : public XiParserInterface {

public:
    AbstractXiParser();
    AbstractXiParser(network_t*);
    virtual ~AbstractXiParser();

    void virtual parse(std::ifstream& input);
    void virtual parse_line(const std::string& line) = 0;
    virtual network_t* results() const = 0;

protected:
    char delim;
};

#endif