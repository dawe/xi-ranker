#ifndef DISTANCE_H
#define DISTANCE_H

#include <algorithm>
#include "collections.hpp"
#ifndef NDEBUG
    #include <iostream>
#endif


struct x_nodes_pair_t {
    const x_node_t a, b;
    double score;
    x_nodes_pair_t(const x_node_t& aa, const x_node_t& bb, double r) : a(aa), b(bb), score(r) {}
    x_nodes_pair_t(const x_nodes_pair_t& o): a(o.a), b(o.b), score(o.score) {}
};

typedef std::list<x_nodes_pair_t> score_list_t;

struct XNodesComp {
    bool operator()(const x_nodes_pair_t& a, const x_nodes_pair_t& b) const {
        return a.score < b.score;
    }
};


// Distance metric interface
class Distance {
public:
    virtual ~Distance() {};
    // void virtual data(const network_t&) = 0;
    // A metric must implement this method, invoked by the csi *metric*.
    double virtual operator()(const network_t& nt, const adj_t& n1, const adj_t& n2) = 0;
};

#endif