#ifndef CSI_DISTANCE_H
#define CSI_DISTANCE_H

#include "distance.hpp"
#include "utils.hpp"

class Csi : public Distance {

public:
    ~Csi();
    Csi(double treshold = 0.05);

    void metric(Distance*);
    double operator()(const network_t& nt, const adj_t& n1, const adj_t& n2);
    // void comp_metric_scores(const network_t& nt, score_list_t& scores);
    // void sort_scores(score_list_t& scores);

private:
    // const network_t* m_co;
    Distance* m_m;
    double m_thr;
    // score_list_t *m_metric_scores;
};

#endif