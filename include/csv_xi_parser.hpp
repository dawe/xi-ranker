#ifndef CSVXIPARSER_H
#define CSVXIPARSER_H

#include "abstract_xi_parser.hpp"

class CsvXiParser : public AbstractXiParser {

public:
    CsvXiParser();
    CsvXiParser(network_t* collection);
    ~CsvXiParser();
    void virtual parse(std::ifstream& input);
    void virtual parse_line(const std::string& line);
    network_t* results() const;

private:
    network_t* co;
};

#endif
