#ifndef XIPARSERINTERFACE_H
#define XIPARSERINTERFACE_H

#include <string>
#include <fstream>
#include <iostream>
#include <stdexcept>
#include "collections.hpp"
#include "utils.hpp"

class XiParserInterface {

public:
    XiParserInterface() {};
    virtual ~XiParserInterface() {};
    // No cctor for now
    XiParserInterface(network_t*) {};
    virtual void parse(std::ifstream& input) = 0;
    virtual void parse_line(const std::string& line) = 0;
    virtual network_t* results() const = 0;
};

#endif