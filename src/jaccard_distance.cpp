#include "jaccard_distance.hpp"

Jaccard::Jaccard() {
    #ifndef NDEBUG
        std::cout << " Jaccard::Jaccard()" << std::endl;
    #endif
}
Jaccard::~Jaccard() {
    #ifndef NDEBUG
        std::cout << " Jaccard::~Jaccard()" << std::endl;
    #endif
}

double Jaccard::operator()(const network_t& nt, const adj_t& n1, const adj_t& n2) {
    #ifndef NDEBUG
        std::cout << " Jaccard::operator()(const network_t& nt, const adj_t& n1, const adj_t& n2) const "
        << " (" << n1.node << ", " << n2.node << ") "  << std::endl;
    #endif

    unsigned size1 = n1.adj.size(), size2 = n2.adj.size(),
        min = size1 < size2 ? size1 : size2;
    y_node_list_t inter(min), u(size1 + size2);

    intersection(n1.adj, n2.adj, inter);
    uni(n1.adj, n2.adj, u);

    return inter.size() / (double)u.size();
}

