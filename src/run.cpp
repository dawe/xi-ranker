#include <unistd.h>
#include <stdlib.h>
#include "run.hpp"

void print_usage() {
    using namespace std;
    cout << " usage: xi.app [<options>] <network_file>" << endl << endl <<
    " By default it prints the results inside the csi_rank.report file." << endl <<
    endl << " Options:" << endl <<
    "\t" << "-p <parser> " << "Specify the input format. Supported formats are csv. (LOL). Default csv." << endl <<
    "\t" << "-m <metric> " << "Specify which metric to use. Supported metrics are pcc, jaccard. Default pcc." << endl <<
    "\t" << "-t <threshold> " << "Specify the threshold for the CSI formula. Default 0.05." << endl << endl <<
    " e.g.:" << endl <<
    "\t" << "./xi.app -p csv -t 0.1 -m jaccard <file_path>" << endl;
}

int main(int argc, const char* argv[]) {
    using namespace std;

    int c, args_num = argc;
    string parser_type, metric_type;

    AbstractXiParser *parser = NULL;
    Distance *metric = NULL;
    Csi csi;
    double threshold = 0.05;

    while ((c = getopt (argc, (char **)argv, "p:t:m:h")) != -1) {
        --args_num;
        switch (c) {
            case 'h':
                print_usage();
                return 0;
            // Parser
            case 'p':
                parser_type = optarg;
                break;
            case 'm':
                metric_type = optarg;
                break;
            case 't':
                threshold = atof(optarg);
                break;
            case '?':
                cerr << "Error: bad formatted command." << endl;
                print_usage();
                return 1;
            default:
                exit(42);
        }
    }

    // It means there aren't non-option arguments.
    if (optind == argc) {
        cerr << "Error: a file path must be provided." << endl;
        print_usage();
        exit(42);
    }

    network_t *network = new network_t();

    try {
        // Detect parser
        if (parser_type == "csv" || parser_type.empty()) {
            parser = new CsvXiParser(network);
        } else if (parser_type == "tsv" || parser_type.empty()) {
            parser = new TsvXiParser(network);
        }

        // Detect metric
        if (metric_type == "jaccard") {
            metric = new Jaccard();
        } else if(metric_type == "pcc") {
            metric = new Pcc();
        } else if (metric_type.empty()) {
            metric = new Pcc();
        } else {
            cerr << "Error: bad formatted command." << endl;
            print_usage();
            exit(42);
        }

        XiRanker xi(argv[optind]);
        csi.metric(metric);
        xi.parser(*parser);
        xi.distance(csi);
        xi.run();
        xi.print_results(cout);
    } catch (const std::exception& e) {
        if (network != NULL) delete network;
        if (parser != NULL) delete parser;
        if (metric != NULL) delete metric;
        cerr << e.what() << endl;
        exit(1);
    }

    return 0;
}
