#include "csv_xi_parser.hpp"

CsvXiParser::CsvXiParser() : co(0) {}
CsvXiParser::CsvXiParser(network_t* collection)
        : co(collection) {
            delim = ',';
        }
CsvXiParser::~CsvXiParser() {}

void CsvXiParser::parse(std::ifstream& input) {
    AbstractXiParser::parse(input);
}

void CsvXiParser::parse_line(const std::string& line) {
    #ifndef NDEBUG
            std::cout << " CsvXiParser::parse_line invoked" << std::endl;
    #endif
    std::vector<std::string> xandys;
    // gene tr1,tr2,...
    split(line, xandys, delim);
    try {
        adj_t a;
        a.node = xandys.at(0);
        a.adj = y_node_list_t(xandys.begin() + 1, xandys.end());
        co->nt.push_back(a);
    } catch(const std::out_of_range& e) {
        // we'll skip this line
        std::cerr << "ERROR: skipping line => " << line << std::endl;
    }
}

network_t* CsvXiParser::results() const {
    return co;
}