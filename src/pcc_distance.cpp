#include "pcc_distance.hpp"
#include <assert.h>

Pcc::Pcc() : total_y_nodes(0) {
    #ifndef NDEBUG
        std::cout << " Pcc::Pcc()" << std::endl;
    #endif
}

Pcc::~Pcc() {
    #ifndef NDEBUG
        std::cout << " Pcc::~Pcc()" << std::endl;
    #endif
}

double Pcc::operator()(const network_t& nt, const adj_t& n1, const adj_t& n2) {
    #ifndef NDEBUG
        std::cout << " Pcc::operator()(const network_t& nt, const adj_t& n1, const adj_t& n2)" << std::endl;
    #endif

    unsigned size1 = n1.adj.size(), size2 = n2.adj.size(),
        min = size1 < size2 ? size1 : size2;
    int n1n2 = n1.adj.size() * n2.adj.size();

    if (!total_y_nodes) {
        y_node_list_t tt;
        y_node_list_t::const_iterator a_i, a_e, res;
        adj_list_t::const_iterator n_i = nt.nt.begin(), n_e = nt.nt.end();
        for (; n_i != n_e; ++n_i) {
            a_i = n_i->adj.begin();
            a_e = n_i->adj.end();
            for (; a_i != a_e; ++a_i) {
                res = std::find(tt.begin(), tt.end(), *a_i);
                if (res == tt.end())
                    tt.push_back(*a_i);
                    // total_y_nodes += 1;
            }
        }
        total_y_nodes = tt.size();
    }

    y_node_list_t inter(min);
    intersection(n1.adj, n2.adj, inter);
    double num = int(inter.size()) * total_y_nodes - n1n2,
        den = sqrt(n1n2 * (total_y_nodes - n1.adj.size()) * (total_y_nodes - n2.adj.size())),
        sc;

    if (den == 0.0) sc = 0.0;
    else sc = num / den;

    return sc;
}