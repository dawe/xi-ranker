#include "xi_ranker.hpp"

const std::string XiRanker::m_output_file_name = "ranking.report";

XiRanker::XiRanker() {
    #ifndef NDEBUG
        std::cout << " XiRanker::XiRanker()" << std::endl;
    #endif
}
XiRanker::XiRanker(const std::string& file_name)
    : m_input_file_name(file_name), m_d(0), m_parser(0), m_scores(0) {
    #ifndef NDEBUG
        std::cout << "" << std::endl;
    #endif
}

void XiRanker::clear() {
    #ifndef NDEBUG
        std::cout << " XiRanker::clear()" << std::endl;
    #endif

    if (m_scores != 0) {
        delete m_scores;
        m_scores = 0;
    }

    m_parser = 0;
    m_d = 0;
}

XiRanker::~XiRanker() {
    #ifndef NDEBUG
        std::cout << " XiRanker::~XiRanker()" << std::endl;
    #endif
    clear();
}

void XiRanker::distance(Distance& dist) {
    #ifndef NDEBUG
        std::cout << " XiRanker::distance(Distance& dist)" << std::endl;
    #endif
    m_d = &dist;
}

void XiRanker::parser(AbstractXiParser& p) {
    #ifndef NDEBUG
        std::cout << " XiRanker::parser(AbstractXiParser& p)" << std::endl;
    #endif
    m_parser = &p;
}

void XiRanker::run() {
    #ifndef NDEBUG
        std::cout << " XiRanker::run()" << std::endl;
    #endif

    // Open the streams
    std::ifstream in(m_input_file_name.c_str());
    if (in.fail()) {
        std::cerr << " XiRanker::run() has failed to open input or output stream"
        << std::endl;
        return;
    }

    // Parse the input file
    m_parser->parse(in);
    // Get the network
    network_t* co = m_parser->results();
    // sort results in order to use set operations.
    for (adj_list_t::iterator ntit = co->nt.begin(), ntend = co->nt.end();
         ntit != ntend; ++ntit) {
        std::sort(ntit->adj.begin(), ntit->adj.end());
    }

    try {
        m_scores = new score_list_t();
        // m_scores->reserve(co->nt.size());
    } catch (std::bad_alloc er) {
        if (m_scores != 0) delete m_scores;
        m_scores = 0;
        throw;
    }

    comp_scores(*m_d, *co, *m_scores);


    // Distance(co);
    // print_rank(out, *rank, size);
    // delete rank;
    // rank = 0;
}

void XiRanker::comp_scores(Distance& m, const network_t& co, score_list_t& scores) {
    #ifndef NDEBUG
        std::cout << " XiRanker::comp_scores(Distance&, const network_t&, score_list_t&)" << std::endl;
    #endif

    network_t::cpair_iterator pb = co.begin(), pe = co.end();
    adj_pair p;
    for (; pb != pe; ++pb) {
        p = *pb;
        scores.push_back(x_nodes_pair_t(p.a->node, p.b->node, m(co, *p.a, *p.b)));
    }
}

void XiRanker::print_results(std::ostream& out) {
    #ifndef NDEBUG
        std::cout << " XiRanker::print_results(std::ofstream& o)" << std::endl;
    #endif

    score_list_t::const_iterator it = m_scores->begin(),
        end = m_scores->end();

    while(it != end) {
        out << it->a << "\t" << it->b << "\t" << it->score << std::endl;
        ++it;
    }
}
