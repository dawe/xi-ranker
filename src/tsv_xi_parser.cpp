#include "tsv_xi_parser.hpp"

TsvXiParser::TsvXiParser() : co(0) {}
TsvXiParser::TsvXiParser(network_t* collection)
        : co(collection) {
            delim = '\t';
        }
TsvXiParser::~TsvXiParser() {}

void TsvXiParser::parse(std::ifstream& input) {
    AbstractXiParser::parse(input);
}

void TsvXiParser::parse_line(const std::string& line) {
    #ifndef NDEBUG
            std::cout << " TsvXiParser::parse_line invoked" << std::endl;
    #endif
    std::vector<std::string> xandys;
    // gene tr1,tr2,...
    split(line, xandys, delim);
    try {
        adj_t a;
        a.node = xandys.at(0);
        a.adj = y_node_list_t(xandys.begin() + 1, xandys.end());
        co->nt.push_back(a);
    } catch(const std::out_of_range& e) {
        // we'll skip this line
        std::cerr << "ERROR: skipping line => " << line << std::endl;
    }
}

network_t* TsvXiParser::results() const {
    return co;
}
