#include "csi_distance.hpp"

Csi::Csi(double treshold) : m_m(0), m_thr(treshold) {}

Csi::~Csi() {
    m_m = 0;
}

void Csi::metric(Distance* mm) {
    m_m = mm;
}

double Csi::operator()(const network_t& nt, const adj_t& n1, const adj_t& n2) {
    #ifndef NDEBUG
        std::cout << " Csi::operator()(const network_t& nt, const adj_t& n1, const adj_t& n2)"
        << std::endl;
    #endif

    adj_list_t::const_iterator xit = nt.nt.begin(), xend = nt.nt.end();
    double d = (*m_m)(nt, n1, n2) - m_thr;
    int comp_node_counter = 2;

    for (; xit != xend; ++xit) {
        #ifndef NDEBUG
            std::cout << "COMP WITH " << (*xit) << std::endl;
        #endif

        // ugly but faster
        if (&(*xit) != &n1 && &(*xit) != &n2) {
            if ((*m_m)(nt, n1, *xit) >= d || (*m_m)(nt, *xit, n2) >= d) {
                ++comp_node_counter;
                #ifndef NDEBUG
                    std::cout << "INC NODE COUNTER TO " << comp_node_counter << std::endl;
                #endif
            }
        }
    }

    return 1 - comp_node_counter / (double) nt.nt.size();
}