#include "abstract_xi_parser.hpp"
#ifndef NDEBUG
    #include <iostream>
#endif

AbstractXiParser::AbstractXiParser() {}
AbstractXiParser::~AbstractXiParser() {}
AbstractXiParser::AbstractXiParser(network_t*) {}
void AbstractXiParser::parse(std::ifstream& input) {
    #ifndef NDEBUG
        std::cout << "AbstractXiParser::parse invoked" << std::endl;
    #endif

    std::string line;

    while(input.good()) {
        std::getline(input, line);
        line = trim(line);
        parse_line(line);
    }
}
