#include "network_it_test.hpp"

void nt_it_test() {
    network_t n;
    make_nt(n);
    adj_list_t::const_iterator c = n.nt.begin(), a = c, b = ++c; ++c;
    network_t::cpair_iterator it = n.begin(), end = n.end();
    assert(it->a == &(*a) && it->b == &(*b));
    ++it;
    assert(it->a == &(*a) && it->b == &(*c));
    ++it;
    assert(it->a == &(*b) && it->b == &(*c));
    assert(++it == end);
}