#include "pcc_test.hpp"

void pcc_test() {
    network_t n;
    make_nt(n);
    Pcc pcc;
    // 1 * 3 - 3 / sqrt(3 * 0) A-B, A-C
    // -1 / 2 B-C
    adj_list_t::const_iterator i = n.nt.begin(), j = i;
    assert(pcc(n, *i, *(++j)) == 0.0);
    assert(pcc(n, *i, *(++j)) == 0.0);
    assert(pcc(n, *(++i), *j) == -0.5);
}