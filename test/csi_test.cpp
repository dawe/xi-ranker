#include "csi_test.hpp"

#define round(n) (round((n) * 10000.0) / 10000.0)

// PCC(A, B) = 0   1 * 3 - 3 / sqrt(3 * 0) A-B, A-C
// PCC(A, C) = 0
// PCC(B, C) = - 0.5  // -1 / 2 B-C

// CSI(A, B) = 1 - 1 / 3 = 0.6667
// CSI(A, C) = 1 - 1 / 3 = 0.6667
// CSI(B, C) = 1 - 1 / 3 = 0.6667

void csi_test_or() {
    network_t n;
    make_nt(n);

    Csi csi;
    Pcc pcc;
    csi.metric(&pcc);
    network_t::cpair_iterator it = n.begin(), end = n.end();
    double v[3] = { 0.6667, 0.6667, 0.6667 }, d;
    for (unsigned i = 0; it != end; ++it, ++i) {
        d = csi(n, *it->a, *it->b);
        print(*it) << " - expected: " << v[i] << " found: " << (round(d)) << std::endl;
        assert(v[i] == round(d));
    }
}

void csi_test_and() {
    network_t n;
    make_nt(n);

    Csi csi;
    Pcc pcc;
    csi.metric(&pcc);
    network_t::cpair_iterator it = n.begin(), end = n.end();
    double v[3] = { 0.0, 0.0, 0.0 }, d;
    for (unsigned i = 0; it != end; ++it, ++i) {
        d = csi(n, *it->a, *it->b);
        print(*it) << " - expected: " << v[i] << " found: " << (round(d)) << std::endl;
        assert(v[i] == round(d));
    }
}