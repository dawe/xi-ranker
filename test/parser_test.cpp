#include "parser_test.hpp"

void
parser_test() {
    pp("start");
    network_t nt, cmp;
    CsvXiParser p(&nt);
    std::ifstream f;
    std::string file_name = "inputs/test.csv";
    make_nt(cmp);

    f.exceptions(std::ios::failbit | std::ios::badbit);

    try {
        f.open(file_name.c_str());
    } catch (std::ifstream::failure& e) {
        std::cerr << (e.what()) << std::endl;
        throw;
    }

    pp("Expect ../inputs/test.csv to be good");
    assert(f.good());
    p.parse(f);
    nt = *p.results();

    pp("Expect the number of genes to be 3");
    assert(nt.nt.size() == 3);
    adj_list_t::const_iterator nit = nt.nt.begin(), cit = cmp.nt.begin();
    y_node_list_t::const_iterator ny, cy;
    pp("Expect the proper network");
    for (; nit != nt.nt.end(); ++nit, ++cit) {
        pp(nit->node + "-" + cit->node);
        assert(nit->node == cit->node);
        for (ny = nit->adj.begin(), cy = cit->adj.begin();
            ny != nit->adj.end(); ++ny, ++cy) {
            assert(*ny == *cy);
        }
    }
}