#ifndef SUPPORT_TEST
#define SUPPORT_TEST

#include <assert.h>
#include <iostream>
#include "../include/collections.hpp"

#define print(str) (std::cout << " " << (str))

#define println(str) (std::cout << " " << (str) << std::endl)

#define pp(str) (std::cout << " " << __FILE__ << ":" << __LINE__ \
    << " => " << (str) << std::endl)

void inline
make_nt(network_t& nt) {
    y_node_list_t ynodes;
    adj_t a;
    a.adj.push_back("Y1"); a.adj.push_back("Y2"); a.adj.push_back("Y3");
    a.node = "A";
    nt.nt.push_back(a);
    a.adj.clear();
    a.node = "B";
    a.adj.push_back("Y2");
    nt.nt.push_back(a);
    a.adj.clear();
    a.node = "C";
    a.adj.push_back("Y3");
    nt.nt.push_back(a);
}

#endif