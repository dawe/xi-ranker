CXX = g++
INC = ./include
TEST = ./test
SRC = ./src
#DEBUGFLAGS = -Wall -Wextra -g -DNDEBUG
CXXFLAGS = -O3 $(DEBUGFLAGS) -DNDEBUG
CPPFLAGS = -I$(INC) -I$(TEST)
OBJS = xi_ranker.o \
	abstract_xi_parser.o \
	csi_distance.o \
	jaccard_distance.o \
	pcc_distance.o \
	csv_xi_parser.o \
	tsv_xi_parser.o
vpath %.hpp $(INC)
vpath %.cpp $(SRC) $(TEST)

xi.app: run.cpp run.hpp utils.hpp collections.hpp $(OBJS)
	$(CXX) $(CPPFLAGS) -g $< $(OBJS) -o xi.app

run_test.app: tests.cpp parser_test.o pcc_test.o network_it_test.o csi_test.o $(OBJS)
	$(CXX) $(DEBUGFLAGS) $(CPPFLAGS) $+ -o $@

.PHONY: clean run all t clean_t

all: clean xi.app run_test.app

run:
	./xi.app inputs/network.csv

t:
	./run_test.app

clean:
	rm -fr *.o *.app *.dSYM

clean_t:
	rm -f *_test.o *_test.app
